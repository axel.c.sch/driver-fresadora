EESchema Schematic File Version 4
LIBS:driver4ejes-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74xx:74HC14 U?
U 1 1 5E4FFACB
P 4300 1900
F 0 "U?" H 4300 2217 50  0000 C CNN
F 1 "74HC14" H 4300 2126 50  0000 C CNN
F 2 "" H 4300 1900 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74HC14" H 4300 1900 50  0001 C CNN
	1    4300 1900
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC14 U?
U 2 1 5E4FFB6B
P 4300 3200
F 0 "U?" H 4300 3517 50  0000 C CNN
F 1 "74HC14" H 4300 3426 50  0000 C CNN
F 2 "" H 4300 3200 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74HC14" H 4300 3200 50  0001 C CNN
	2    4300 3200
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC14 U?
U 3 1 5E4FFBA8
P 7600 1900
F 0 "U?" H 7600 2217 50  0000 C CNN
F 1 "74HC14" H 7600 2126 50  0000 C CNN
F 2 "" H 7600 1900 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74HC14" H 7600 1900 50  0001 C CNN
	3    7600 1900
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC14 U?
U 4 1 5E4FFBEF
P 7600 3200
F 0 "U?" H 7600 3517 50  0000 C CNN
F 1 "74HC14" H 7600 3426 50  0000 C CNN
F 2 "" H 7600 3200 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74HC14" H 7600 3200 50  0001 C CNN
	4    7600 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:R 10K
U 1 1 5E4FFD04
P 3450 1600
F 0 "10K" H 3520 1646 50  0000 L CNN
F 1 "R" H 3520 1555 50  0000 L CNN
F 2 "" V 3380 1600 50  0001 C CNN
F 3 "~" H 3450 1600 50  0001 C CNN
	1    3450 1600
	1    0    0    -1  
$EndComp
$Comp
L Device:R 1K
U 1 1 5E4FFD6A
P 3750 1900
F 0 "1K" V 3543 1900 50  0000 C CNN
F 1 "R" V 3634 1900 50  0000 C CNN
F 2 "" V 3680 1900 50  0001 C CNN
F 3 "~" H 3750 1900 50  0001 C CNN
	1    3750 1900
	0    1    1    0   
$EndComp
$Comp
L Device:C 0.1uF
U 1 1 5E4FFE86
P 3450 2050
F 0 "0.1uF" H 3565 2096 50  0000 L CNN
F 1 "C" H 3565 2005 50  0000 L CNN
F 2 "" H 3488 1900 50  0001 C CNN
F 3 "~" H 3450 2050 50  0001 C CNN
	1    3450 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 1750 3450 1900
Wire Wire Line
	3600 1900 3450 1900
Connection ~ 3450 1900
$Comp
L power:GND #PWR?
U 1 1 5E4FFFB7
P 3450 2250
F 0 "#PWR?" H 3450 2000 50  0001 C CNN
F 1 "GND" H 3455 2077 50  0000 C CNN
F 2 "" H 3450 2250 50  0001 C CNN
F 3 "" H 3450 2250 50  0001 C CNN
	1    3450 2250
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR?
U 1 1 5E4FFFFC
P 3450 1400
F 0 "#PWR?" H 3450 1250 50  0001 C CNN
F 1 "VCC" H 3467 1573 50  0000 C CNN
F 2 "" H 3450 1400 50  0001 C CNN
F 3 "" H 3450 1400 50  0001 C CNN
	1    3450 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 1400 3450 1450
Wire Wire Line
	3450 2200 3450 2250
$Comp
L Device:R 10K?
U 1 1 5E500375
P 3450 2900
F 0 "10K?" H 3520 2946 50  0000 L CNN
F 1 "R" H 3520 2855 50  0000 L CNN
F 2 "" V 3380 2900 50  0001 C CNN
F 3 "~" H 3450 2900 50  0001 C CNN
	1    3450 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:R 1K?
U 1 1 5E50037C
P 3750 3200
F 0 "1K?" V 3543 3200 50  0000 C CNN
F 1 "R" V 3634 3200 50  0000 C CNN
F 2 "" V 3680 3200 50  0001 C CNN
F 3 "~" H 3750 3200 50  0001 C CNN
	1    3750 3200
	0    1    1    0   
$EndComp
$Comp
L Device:C 0.1uF?
U 1 1 5E500383
P 3450 3350
F 0 "0.1uF?" H 3565 3396 50  0000 L CNN
F 1 "C" H 3565 3305 50  0000 L CNN
F 2 "" H 3488 3200 50  0001 C CNN
F 3 "~" H 3450 3350 50  0001 C CNN
	1    3450 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 3050 3450 3200
Wire Wire Line
	3600 3200 3450 3200
Connection ~ 3450 3200
$Comp
L power:GND #PWR?
U 1 1 5E50038D
P 3450 3550
F 0 "#PWR?" H 3450 3300 50  0001 C CNN
F 1 "GND" H 3455 3377 50  0000 C CNN
F 2 "" H 3450 3550 50  0001 C CNN
F 3 "" H 3450 3550 50  0001 C CNN
	1    3450 3550
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR?
U 1 1 5E500393
P 3450 2700
F 0 "#PWR?" H 3450 2550 50  0001 C CNN
F 1 "VCC" H 3467 2873 50  0000 C CNN
F 2 "" H 3450 2700 50  0001 C CNN
F 3 "" H 3450 2700 50  0001 C CNN
	1    3450 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 2700 3450 2750
Wire Wire Line
	3450 3500 3450 3550
Wire Wire Line
	3450 3200 3000 3200
$Comp
L Device:R 10K?
U 1 1 5E5004CD
P 6750 2900
F 0 "10K?" H 6820 2946 50  0000 L CNN
F 1 "R" H 6820 2855 50  0000 L CNN
F 2 "" V 6680 2900 50  0001 C CNN
F 3 "~" H 6750 2900 50  0001 C CNN
	1    6750 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:R 1K?
U 1 1 5E5004D4
P 7050 3200
F 0 "1K?" V 6843 3200 50  0000 C CNN
F 1 "R" V 6934 3200 50  0000 C CNN
F 2 "" V 6980 3200 50  0001 C CNN
F 3 "~" H 7050 3200 50  0001 C CNN
	1    7050 3200
	0    1    1    0   
$EndComp
$Comp
L Device:C 0.1uF?
U 1 1 5E5004DB
P 6750 3350
F 0 "0.1uF?" H 6865 3396 50  0000 L CNN
F 1 "C" H 6865 3305 50  0000 L CNN
F 2 "" H 6788 3200 50  0001 C CNN
F 3 "~" H 6750 3350 50  0001 C CNN
	1    6750 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 3050 6750 3200
Wire Wire Line
	6900 3200 6750 3200
Connection ~ 6750 3200
$Comp
L power:GND #PWR?
U 1 1 5E5004E5
P 6750 3550
F 0 "#PWR?" H 6750 3300 50  0001 C CNN
F 1 "GND" H 6755 3377 50  0000 C CNN
F 2 "" H 6750 3550 50  0001 C CNN
F 3 "" H 6750 3550 50  0001 C CNN
	1    6750 3550
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR?
U 1 1 5E5004EB
P 6750 2700
F 0 "#PWR?" H 6750 2550 50  0001 C CNN
F 1 "VCC" H 6767 2873 50  0000 C CNN
F 2 "" H 6750 2700 50  0001 C CNN
F 3 "" H 6750 2700 50  0001 C CNN
	1    6750 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 2700 6750 2750
Wire Wire Line
	6750 3500 6750 3550
Wire Wire Line
	6750 3200 6300 3200
$Comp
L Device:R 10K?
U 1 1 5E50060A
P 6750 1600
F 0 "10K?" H 6820 1646 50  0000 L CNN
F 1 "R" H 6820 1555 50  0000 L CNN
F 2 "" V 6680 1600 50  0001 C CNN
F 3 "~" H 6750 1600 50  0001 C CNN
	1    6750 1600
	1    0    0    -1  
$EndComp
$Comp
L Device:R 1K?
U 1 1 5E500611
P 7050 1900
F 0 "1K?" V 6843 1900 50  0000 C CNN
F 1 "R" V 6934 1900 50  0000 C CNN
F 2 "" V 6980 1900 50  0001 C CNN
F 3 "~" H 7050 1900 50  0001 C CNN
	1    7050 1900
	0    1    1    0   
$EndComp
$Comp
L Device:C 0.1uF?
U 1 1 5E500618
P 6750 2050
F 0 "0.1uF?" H 6865 2096 50  0000 L CNN
F 1 "C" H 6865 2005 50  0000 L CNN
F 2 "" H 6788 1900 50  0001 C CNN
F 3 "~" H 6750 2050 50  0001 C CNN
	1    6750 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 1750 6750 1900
Wire Wire Line
	6900 1900 6750 1900
Connection ~ 6750 1900
$Comp
L power:GND #PWR?
U 1 1 5E500622
P 6750 2250
F 0 "#PWR?" H 6750 2000 50  0001 C CNN
F 1 "GND" H 6755 2077 50  0000 C CNN
F 2 "" H 6750 2250 50  0001 C CNN
F 3 "" H 6750 2250 50  0001 C CNN
	1    6750 2250
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR?
U 1 1 5E500628
P 6750 1400
F 0 "#PWR?" H 6750 1250 50  0001 C CNN
F 1 "VCC" H 6767 1573 50  0000 C CNN
F 2 "" H 6750 1400 50  0001 C CNN
F 3 "" H 6750 1400 50  0001 C CNN
	1    6750 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 1400 6750 1450
Wire Wire Line
	6750 2200 6750 2250
Wire Wire Line
	6750 1900 6300 1900
Wire Wire Line
	3900 1900 4000 1900
Wire Wire Line
	3900 3200 4000 3200
Wire Wire Line
	7200 3200 7300 3200
Wire Wire Line
	7200 1900 7300 1900
Wire Wire Line
	3000 2200 3450 2200
Connection ~ 3450 2200
Wire Wire Line
	3000 3550 3450 3550
Connection ~ 3450 3550
Wire Wire Line
	6300 3550 6750 3550
Connection ~ 6750 3550
Wire Wire Line
	6300 2250 6750 2250
Connection ~ 6750 2250
Wire Wire Line
	4600 1900 4700 1900
Wire Wire Line
	4600 3200 4700 3200
Wire Wire Line
	7900 3200 8000 3200
Wire Wire Line
	8000 1900 7900 1900
Text HLabel 3000 1900 0    50   Input ~ 0
FC_1_IN
Text HLabel 3000 2200 0    50   Input ~ 0
FC_1_GND
Wire Wire Line
	3000 1900 3450 1900
Text HLabel 3000 3200 0    50   Input ~ 0
FC_2_IN
Text HLabel 3000 3550 0    50   Input ~ 0
FC_2_GND
Text HLabel 4700 1900 2    50   Input ~ 0
FC_1_OUT
Text HLabel 4700 3200 2    50   Input ~ 0
FC_2_OUT
Text HLabel 6300 1900 0    50   Input ~ 0
FC_3_IN
Text HLabel 6300 2250 0    50   Input ~ 0
FC_3_GND
Text HLabel 6300 3200 0    50   Input ~ 0
FC_4_IN
Text HLabel 6300 3550 0    50   Input ~ 0
FC_4_GND
Text HLabel 8000 1900 2    50   Input ~ 0
FC_3_OUT
Text HLabel 8000 3200 2    50   Input ~ 0
FC_4_OUT
$EndSCHEMATC
